package ru.tsc.gulin.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
