package ru.tsc.gulin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-create";

    @NotNull
    public static final String DESCRIPTION = "Create new task";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("ENTER START DATE:");
        @Nullable final Date dateStart = TerminalUtil.nextDate();
        System.out.println("ENTER END DATE:");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final String userId = getUserId();
        getTaskService().create(userId, name, description, dateStart, dateEnd);
    }

}
