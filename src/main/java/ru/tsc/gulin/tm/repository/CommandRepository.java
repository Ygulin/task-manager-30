package ru.tsc.gulin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.ICommandRepository;
import ru.tsc.gulin.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @NotNull final String name = command.getName();
        if (!name.isEmpty()) mapByName.put(name, command);
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandsWithArgument() {
        return mapByArgument.values();
    }

}
