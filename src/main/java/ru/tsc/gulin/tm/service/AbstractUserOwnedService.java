package ru.tsc.gulin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gulin.tm.api.repository.IUserOwnedRepository;
import ru.tsc.gulin.tm.api.service.IUserOwnedService;
import ru.tsc.gulin.tm.enumerated.Sort;
import ru.tsc.gulin.tm.exception.field.IdEmptyException;
import ru.tsc.gulin.tm.exception.field.IndexIncorrectException;
import ru.tsc.gulin.tm.exception.field.UserIdEmptyException;
import ru.tsc.gulin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (comparator == null) return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (model == null) return null;
        return repository.add(userId, model);
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(index).filter(item -> item > -1).orElseThrow(IndexIncorrectException::new);
        return repository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (model == null) return null;
        return repository.remove(userId, model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(index).filter(item -> item > -1).orElseThrow(IndexIncorrectException::new);
        return repository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        return repository.removeById(userId, id);
    }

    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        return repository.existsById(userId, id);
    }

    @Override
    public long getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        return repository.getSize(userId);
    }

}
